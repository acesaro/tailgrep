#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pcre.h>

//BEGIN CPP

#define BUFFERCHUNK 1024

//END CPP

// BEGIN Function prototypes

int regexMatch (char *line, const char *regexPattern);

// END Function prototypes

// BEGIN Functions

void
readPrint (FILE *inputFile, char *readBuffer, char *regexPattern)
{
  const char *error;
  int errorOffset;
  pcre *reCompiled;
  unsigned int lineLength;
  int ovector[1];
  int regexMatchResult;
  char *line;

  reCompiled = pcre_compile(regexPattern, 0, &error, &errorOffset, NULL);

  if (reCompiled == NULL)
  {
    printf("pcre_compile failed (offset: %d), %s\n", errorOffset, error);
  }

  if (inputFile)
  {
    unsigned long curOffset = ftell(inputFile);
    size_t nread;

    while (1)
    {
      while ((nread = fread(readBuffer, 1, CHUNK, inputFile)) > 0)
      {
        while (fgets(readBuffer, CHUNK, inputFile) != NULL)
        {
          lineLength = (int)strlen(readBuffer);
          regexMatchResult = pcre_exec(reCompiled, NULL, readBuffer, lineLength, 0, 0, ovector, 1);
          printf("Matches = %d", regexMatchResult);
          fwrite(readBuffer, 1, nread, stdout);
          curOffset = ftell(inputFile);
          printf("%d\n", curOffset);
        }
        sleep(1);
      }
    }
    fclose(inputFile);
  }
}

char
*readLine(FILE *inputFile)
{
  char *readBuffer;
  char currentChar;
  int count = 0;
  int maxLength = BUFFERCHUNK;

  if(inputFile == NULL)
  {
    printf("Error: Null pointer to file passed");
    exit(1);
  }

  readBuffer = (*char)malloc(sizeof(char) * maxLength);

  if(readBuffer == NULL)
  {
    printf("Error: Could not allocate memory for readBuffer");
    exit(1);
  }

  currentChar = fgetc(inputFile);
  
  while((currentChar != '\n') && (currentChar != EOF))
  {
    if(count == maxLength)
    {
      maxLength += BUFFERCHUNK;
      readBuffer = realloc(readBuffer, maxLength);
      if(readBuffer == NULL)
      {
        printf("Error: Could not allocate memory for readBuffer");
        exit(1);
      }
    }
    
    readBuffer[count] = currentChar;
    count++;

    currentChar = fgetc(inputFile);
  }






// END functions

// BEGIN main
int
main (int argc, char *argv[])
{   

  char *filename = NULL;
  char *pattern = NULL;
  FILE *ifp;


  if (argc - 1 >= 1) 
  {
    if (argc - 1 < 1) 
    {
      fprintf(stderr, "Usage: %s <filename> <pattern>\n", argv[0]);
      return 1;
    } 

    while (--argc > 0)
    {
      filename = argv[1];
      pattern = argv[2];
    }

    ifp = fopen(filename, "r");
  }
  else
  {
    filename = "stdin";
    ifp = stdin;
  }

  if (ifp == NULL)
  {
    fprintf(stderr, "File open error.\n");
    exit(1);
  }

  if (readBuffer == NULL)
  {
    fprintf(stderr, "Could not allocate read buffer.\n");
    exit(1);
  }

  printf ("File being concatenated: %s\n", filename);
  printf ("Pattern used for filter: %s\n\n", pattern);
  readPrint (ifp, readBuffer, pattern);

  free (readBuffer);
  
  return 0;
}
// END main
